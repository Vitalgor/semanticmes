# -*- coding: utf-8 -*-
"""

@author: vitalii_kondratiuk
"""

import json

path_to_types = 'types/'

def create_object(Id, description='', obj_type=None):
    """Form a json object.


    Keyword arguments:
    obj_type -- if object has particular type it will be initialized properly
    (default None)
    Id -- unique name
    description -- human-readable text (default empty)
    """
    obj = dict()
    obj["id"] = Id
    obj['description'] = description
    obj['type'] = 'Object'
    if obj_type is not None:
        obj['type'] = obj_type
    if obj_type is not None:
        template = get_json(obj_type+'Type')
        if not isinstance(template['properties'], list):
            template['properties'] = [template['properties']]
        for prop in template['properties']:
            name = prop['id']
            prop_type = prop['type']
            if prop_type == 'string':
                obj[name] = ''
            else:
                obj[name] = create_object(Id+'.'+name, 'Property '+name+
                                           ' of object '+Id,prop_type)
                
    #TODO: if type is not None, add more default properties
    
    
    return obj
    
def add_properties(obj, key, props, replace=False):
    """Add properties to json object.


    Keyword arguments:
    type -- if object has particular type it will be initialized properly
    (default None)
    obj -- object to add the properties to
    key -- key for properties
    props -- object or list of objects
    replace -- if True, new property will replace previous, if exists
    else will append to existing
    """
    if isinstance(props, list) and len(props) == 1:
        props = props[0]
    if key not in obj.keys():
        obj[key] = props
    else:
        if replace:
            obj[key] = props
        else:
            if not isinstance(props, list):
                props = [props]
            if isinstance(obj[key], list):
                obj[key] += props
            else:
                obj[key] = [obj[key]] + props
                
    return obj

def get_json(Id):
    return load_from_file(path_to_types + Id + '.json')

def load_from_file(file):
    """Load json object from file.

get
    Keyword arguments:
    file -- file name as string
    """
    with open(file) as json_file:
        obj = json.load(json_file)
    return obj

def save_to_file(obj, file=None):
    """Save json object to file.


    Keyword arguments:
    obj -- json object
    file -- file name as string. If None, Id is used
    """
    if file is None:
        file = obj['id'] + '.json'
    with open(file, 'w+') as outfile:
        json.dump(obj, outfile)
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    