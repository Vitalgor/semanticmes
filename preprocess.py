import os
from collections import deque
import data_formats_lib

EPS = 1e-2

def build_graph(path):
    material_files = [os.path.join(path, f) for f in os.listdir(path) if 
                      os.path.isfile(os.path.join(path, f)) 
                      and f.find('Material') != -1]
    operation_files = [os.path.join(path, f) for f in os.listdir(path) if 
                      os.path.isfile(os.path.join(path, f)) 
                      and f.find('Operation') != -1]
    operations = [data_formats_lib.load_from_file(f) for f in operation_files]
    materials = [data_formats_lib.load_from_file(f) for f in material_files]
    products = [m for m in materials if float(m['demand']) > 0]
    # есть 2 типа вершин - материалы и операции. Из материалов ведут рёбра в 
    # операции, которые их потребляют. Из операций в те материалы, которые
    # они производят
    graph = dict()
    queue = deque()
    for prod in products:
        graph[prod['id']] = dict()
        graph[prod['id']]['vertex'] = prod
        graph[prod['id']]['edges'] = []
        graph[prod['id']]['type'] = 'material'
        queue.append(prod['id'])
    
    # в начале добавляем все вершины
    materials_all = {m['id']: m for m in materials}
    while len(queue) > 0:
        vertex = queue.pop()
        for operation in operations:
            products = operation['ProductSpecification']
            if not isinstance(products, list):
                products = [products]
            materials = operation['MaterialSpecification']
            if not isinstance(materials, list):
                materials = [materials]
            operation_added = False
            for product in products:
                name = product['id'].split('.')[-1]
                #print(vertex,' ',operation['id'],' ',name)
                if name == vertex:
                    graph[operation['id']] = dict()
                    graph[operation['id']]['vertex'] = operation
                    graph[operation['id']]['edges'] = []
                    graph[operation['id']]['type'] = 'operation'
                    operation_added = True
                    break
                
            if operation_added:
                for product in products:
                    name = product['id'].split('.')[-1]
                    if name not in graph.keys():
                        name = product['id'].split('.')[-1]
                        graph[name] = dict()
                        graph[name]['vertex'] = materials_all[name]
                        graph[name]['edges'] = []
                        graph[name]['type'] = 'material'
                        queue.append(name)
                for material in materials:
                    name = material['id'].split('.')[-1]
                    if name not in graph.keys():
                        graph[name] = dict()
                        graph[name]['vertex'] = materials_all[name]
                        graph[name]['edges'] = []
                        graph[name]['type'] = 'material'
                        queue.append(name)
    
    #добавим рёбра
    
    for k, v in graph.items():
        if v['type'] == 'operation':
            operation = v['vertex']
            products = operation['ProductSpecification']
            if not isinstance(products, list):
                products = [products]
            materials = operation['MaterialSpecification']
            if not isinstance(materials, list):
                materials = [materials]
            for product in products:
                name = product['id'].split('.')[-1]
                v['edges'].append(name)
            for material in materials:
                name = material['id'].split('.')[-1]
                graph[name]['edges'].append(k)  
                    
    return graph

#status 0 -> plan is correct
#status > 0 -> plan is invalid
def check_plan(plan, graph, path):
    units_files = [os.path.join(path, f) for f in os.listdir(path) if 
                      os.path.isfile(os.path.join(path, f))
                      and f.find('Equipment') != -1]
    units = [data_formats_lib.load_from_file(f) for f in units_files]
    units = {u['id']: u for u in units}
    segment_files = [os.path.join(path, f) for f in os.listdir(path) if 
                      os.path.isfile(os.path.join(path, f))
                      and f.find('ProcessSegment') != -1]
    segments = [data_formats_lib.load_from_file(f) for f in segment_files]
    segments = {s['id']:s for s in segments}
    
    material_files = [os.path.join(path, f) for f in os.listdir(path) if 
                      os.path.isfile(os.path.join(path, f)) 
                      and f.find('Material') != -1]
    materials = [data_formats_lib.load_from_file(f) for f in material_files]
    
    # check jobs separately
    for job in plan['Job']:
        name = job['description']
        operation = graph[name]['vertex']
        equipment = units[job['Equipment']]
        process_segment = operation['ProcessSegmentId']
        equipment_class = segments[process_segment]['EquipmentSegmentSpecification']
        if operation['id'] != name:
            return 1
        if equipment['EquipmentClass'] != equipment_class:
            return 2
        if float(job['Batch size']) < float(equipment['min batch size']):
            return 3
        if float(job['Batch size']) > float(equipment['max batch size']):
            return 4
        if float(job['End time']) - float(job['Start time']) != float(operation['Duration']):
            return 5
    
    # check unparallel constrains - each unit is processing not more than one job per 
    # each time unit
    
    for unit in units.values():
        for job in plan['Job']:
            times = []
            if job['Equipment'] == unit['id']:
                times.append((float(job['Start time']), float(job['End time'])))
            times = sorted(times)
            for i in range(1, len(times)):
                if times[i][0] < times[i-1][1]:
                    return 6
    
    # check stock balance and demands
    
    for material in materials:
        times = []
        for job in plan['Job']:
            name = job['description']
            operation = graph[name]['vertex']
            consuming = operation['MaterialSpecification']
            if not isinstance(consuming, list):
                consuming = [consuming]
            for cons in consuming:
                quantity = float(cons['ValueString'])
                name = cons['id'].split('.')[-1]
                if name == material['id']:
                    times.append((float(job['Start time']), -quantity*float(job['Batch size'])))
            
            producting = operation['ProductSpecification']
            if not isinstance(producting, list):
                producting = [producting]
            for product in producting:
                quantity = float(product['ValueString'])
                name = product['id'].split('.')[-1]
                if name == material['id']:
                    times.append((float(job['End time']), quantity*float(job['Batch size'])))
        
        times = sorted(times)
        stock = float(material['initial stock'])
        for i in range(len(times)):
            time = times[i]
            stock += time[1]
            # check only last entry of time
            if i == len(times) - 1 or times[i][0] != times[i+1][0]:
                if stock < -EPS:
                    return 7
                if stock > float(material['max stock']) + EPS:
                    return 8
        if stock < float(material['demand']) - EPS:
            return 9
    
    return 0
    