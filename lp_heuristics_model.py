# -*- coding: utf-8 -*-
"""

@author: t_pro
"""
import os
import plotly.express as px
import pandas as pd

from ortools.linear_solver import pywraplp
import data_formats_lib


def build_simple_discrete_model(graph, path, max_time, time_step=1):
    model = pywraplp.Solver.CreateSolver('SCIP')
    
    process_variables = dict()
    
    units_files = [os.path.join(path, f) for f in os.listdir(path) if 
                      os.path.isfile(os.path.join(path, f))
                      and f.find('Equipment') != -1]
    units = [data_formats_lib.load_from_file(f) for f in units_files]
    
    segment_files = [os.path.join(path, f) for f in os.listdir(path) if 
                      os.path.isfile(os.path.join(path, f))
                      and f.find('ProcessSegment') != -1]
    segments = [data_formats_lib.load_from_file(f) for f in segment_files]
    segments = {s['id']:s for s in segments}
    
    for u in units:
        process_variables[u['id']] = dict()
    
    for v in graph.values():
        if v['type'] == 'operation':
            operation = v['vertex']
            segment = operation['ProcessSegmentId']
            equipment_class = segments[segment]['EquipmentSegmentSpecification']
            for u in units:
                if u['EquipmentClass'] == equipment_class:
                    process_variables[u['id']][operation['id']] = dict()
                    t = 0
                    min_b = float(u['min batch size'])
                    max_b = float(u['max batch size'])
                    while t <= max_time:
                        process_variables[u['id']][operation['id']][t] = dict()
                        variables = process_variables[u['id']][operation['id']][t]
                        name = u['id']+'-'+operation['id']+'-'+str(t)
                        variables['binary'] = model.IntVar(0, 1, name=name+'-bin')
                        variables['batch'] = model.NumVar(0, model.infinity(), name=name+'-batch')
                        
                        t += time_step
        if v['type'] == 'material':
            process_variables[v['vertex']['id']] = dict()
            t = 0
            while t <= max_time:
                max_s = int(v['vertex']['max stock'])
                name = v['vertex']['id']+'-'+str(t)+'-stock'
                process_variables[v['vertex']['id']][t] = model.NumVar(0, max_s, name=name)
                
                t += time_step
    
    makespan = model.NumVar(0, model.infinity(), name='makespan')
    model.Minimize(makespan)
    
    #add constraints
    for t in range(0, max_time, time_step):
        for v in graph.values():
            if v['type'] == 'operation':
                operation = v['vertex']
                segment = operation['ProcessSegmentId']
                equipment_class = segments[segment]['EquipmentSegmentSpecification']
                for u in units:
                    if u['EquipmentClass'] == equipment_class:
                        #makespan
                        x = process_variables[u['id']][operation['id']][t]['binary']
                        model.Add(makespan >= t*x + int(operation['Duration']) - 1)
                        
                        min_b = float(u['min batch size'])
                        max_b = float(u['max batch size'])
                        model.Add(x*min_b <= \
                            process_variables[u['id']][operation['id']][t]['batch'])
                        model.Add(process_variables[u['id']][operation['id']][t]['batch'] <=\
                            x*max_b)
                                
    #stock balance
    material_flow = dict()
    for v in graph.values():
        if v['type'] == 'material':
            material_flow[v['vertex']['id']] = dict()
            for t in range(0, max_time, time_step):
                material_flow[v['vertex']['id']][t] = 0
            
    for name, v in graph.items():
        #add inflow
        if v['type'] == 'operation':
            operation = v['vertex']
            segment = operation['ProcessSegmentId']
            equipment_class = segments[segment]['EquipmentSegmentSpecification']
            for product in v['edges']:
                field_name = name+'.ProductSpecification.'+product
                quantities = operation['ProductSpecification']
                if not isinstance(quantities, list):
                    quantities = [quantities]
                for quantity in quantities:
                    if quantity['id'] == field_name:
                        q = float(quantity['ValueString'])
                        for t in range(0, max_time, time_step):
                            t_begin = t - int(operation['Duration'])
                            if t_begin < 0:
                                continue
                            t_begin -= (t_begin % time_step)
                            for u in units:
                                if u['EquipmentClass'] == equipment_class:
                                    batch = process_variables[u['id']][operation['id']][t_begin]['batch']
                                    material_flow[product][t] = material_flow[product][t] + q*batch
        #add outflow    
        else:
            material = v['vertex']  
            for t in range(0, max_time, time_step):
                for operation_name in v['edges']:
                    operation = graph[operation_name]['vertex']
                    segment = operation['ProcessSegmentId']
                    equipment_class = segments[segment]['EquipmentSegmentSpecification']
                    field_name = operation_name+'.MaterialSpecification.'+name
                    quantities = operation['MaterialSpecification']
                    if not isinstance(quantities, list):
                        quantities = [quantities]
                    for quantity in quantities:
                        if quantity['id'] == field_name:
                            q = float(quantity['ValueString'])
                            for u in units:
                                if u['EquipmentClass'] == equipment_class:
                                    batch = process_variables[u['id']][operation['id']][t]['batch']
                                    material_flow[name][t] = material_flow[name][t] - q*batch
                                        
    for name, v in graph.items():
        #demand constraints
        if v['type'] == 'material':
            material_flow[v['vertex']['id']][0] = material_flow[v['vertex']['id']][0] + float(v['vertex']['initial stock'])
            for t in range(time_step, max_time, time_step):
                model.Add(process_variables[v['vertex']['id']][t] == \
                    process_variables[v['vertex']['id']][t-time_step] + material_flow[name][t])
                
            model.Add(process_variables[v['vertex']['id']][max_time-time_step] >= float(v['vertex']['demand']))
            model.Add(process_variables[v['vertex']['id']][0] == material_flow[v['vertex']['id']][0])
                
        
    
    #non-parallel constraints
    for u in units:
        for t in range(0, max_time, time_step):
            expression = 0
            for name, v in graph.items():
                if v['type'] == 'operation':
                    operation = graph[name]['vertex']
                    segment = operation['ProcessSegmentId']
                    equipment_class = segments[segment]['EquipmentSegmentSpecification']
                    if u['EquipmentClass'] == equipment_class:
                        duration = int(operation['Duration'])
                        for t_begin in range(0, t+1, time_step):
                            if t_begin+duration > t:
                                expression = expression + process_variables[u['id']][operation['id']][t_begin]['binary'] 
            if not isinstance(expression, int):
                model.Add(expression <= 1)
    
    return model, process_variables

def build_lp_heuristic_model(first_stage_vars, graph, path, max_time):
    model = pywraplp.Solver.CreateSolver('SCIP')
    
    process_variables = dict()
    
    units_files = [os.path.join(path, f) for f in os.listdir(path) if 
                      os.path.isfile(os.path.join(path, f))
                      and f.find('Equipment') != -1]
    units = [data_formats_lib.load_from_file(f) for f in units_files]
    
    segment_files = [os.path.join(path, f) for f in os.listdir(path) if 
                      os.path.isfile(os.path.join(path, f))
                      and f.find('ProcessSegment') != -1]
    segments = [data_formats_lib.load_from_file(f) for f in segment_files]
    segments = {s['id']:s for s in segments}
    
    tasks = dict()
    
    makespan = model.NumVar(0, model.infinity(), name='makespan')
    model.Minimize(makespan)
    
    material_flow = dict()
    for v in graph.values():
        if v['type'] == 'material':
            material_flow[v['vertex']['id']] = dict()
            process_variables[v['vertex']['id']] = dict()
            for t in range(0, max_time):
                max_s = int(v['vertex']['max stock'])
                name = v['vertex']['id']+'-'+str(t)+'-stock'
                process_variables[v['vertex']['id']][t] = model.NumVar(0, max_s, name=name)
                material_flow[v['vertex']['id']][t] = float(v['vertex']['initial stock'])
               
    for u in units:
        process_variables[u['id']] = dict()
        tasks[u['id']] = []
        for name, j in first_stage_vars[u['id']].items():
            j_list = [(i, j[i]['batch'].solution_value()) for i in range(max_time) if i in j.keys() and j[i]['binary'].solution_value()]
            for t in j_list:
                tasks[u['id']] += [(t[0], t[1], graph[name]['vertex'])]
        tasks[u['id']] = sorted(tasks[u['id']])
        bound = 0
        n = 0
        for task in tasks[u['id']]:
            process_variables[u['id']][n] = dict()
            name = u['id']+'-'+task[2]['id']+'-'+str(n)
            process_variables[u['id']][n]['start'] = model.NumVar(0, model.infinity(), name=name+'-start')
            process_variables[u['id']][n]['finish'] = model.NumVar(0, model.infinity(), name=name+'-finish')
            
            # правильность времени окончания процесса
            model.Add(process_variables[u['id']][n]['finish'] == \
                process_variables[u['id']][n]['start'] + int(task[2]['Duration']))
            model.Add(makespan >= process_variables[u['id']][n]['finish'])
            
            if n > 0:
                model.Add(process_variables[u['id']][n]['start'] >= process_variables[u['id']][n-1]['finish'])
            
            for t in range(max_time):
                name = u['id']+'-'+task[2]['id']+'-'+str(n)+'-'+str(t)
                process_variables[u['id']][n][t] = dict()
                process_variables[u['id']][n][t]['start'] = model.IntVar(0, 1, name=name+'-st_flag')
                process_variables[u['id']][n][t]['finish'] = model.IntVar(0, 1, name=name+'-fin_flag')
                
                # ограничения на время начала и конца
                # процесс точно не начался позже, чем в плане, который есть
                # и раньше, чем сумма длительностей предыдущих
                if t < bound:
                    model.Add(process_variables[u['id']][n][t]['start'] == 0)
                if t >= task[0]:
                    model.Add(process_variables[u['id']][n][t]['start'] == 1)
                # то же самое про время окончания
                if t < bound + int(task[2]['Duration']):
                    model.Add(process_variables[u['id']][n][t]['finish'] == 0)
                if t >= task[0] + int(task[2]['Duration']):
                    model.Add(process_variables[u['id']][n][t]['finish'] == 1)
                    
                # в остальные временные промежутки эта переменная является
                # заявленным индикатором
                
                if t >= bound and t < task[0]:
                    model.Add(process_variables[u['id']][n][t]['start']  <= \
                        (t - process_variables[u['id']][n]['start']) / max_time + 1)
                    model.Add(process_variables[u['id']][n][t]['start']  >= \
                        (t - process_variables[u['id']][n]['start'] + 1) / max_time)
                        
                if t >= bound + int(task[2]['Duration']) and t < task[0] + int(task[2]['Duration']):
                    model.Add(process_variables[u['id']][n][t]['finish']  <= \
                        (t - process_variables[u['id']][n]['finish']) / max_time + 1)
                    model.Add(process_variables[u['id']][n][t]['finish']  >= \
                        (t - process_variables[u['id']][n]['finish'] + 1) / max_time)
                        
                # склад
                operation = task[2]
                materials = operation['MaterialSpecification']
                if not isinstance(materials, list):
                    materials = [materials]
                for material in materials:
                    quantity = float(material['ValueString'])
                    name = material['id'].split('.')[-1]
                    material_flow[name][t] = material_flow[name][t] - \
                        quantity*task[1]*process_variables[u['id']][n][t]['start']
                
                products = operation['ProductSpecification']
                if not isinstance(products, list):
                    products = [products]
                for product in products:
                    quantity = float(product['ValueString'])
                    name = product['id'].split('.')[-1]
                    material_flow[name][t] = material_flow[name][t] + \
                        quantity*task[1]*process_variables[u['id']][n][t]['finish']
                
            bound += int(task[2]['Duration'])
            n += 1
    
    for name, v in graph.items():
        #demand constraints
        if v['type'] == 'material':
            for t in range(0, max_time):
                model.Add(process_variables[name][t] == material_flow[name][t])
                #print(material_flow[name][t],'\n',name,'\n\n')
            
            model.Add(process_variables[v['vertex']['id']][max_time-1] >= float(v['vertex']['demand']))
    
    return model, process_variables, tasks


def simple_discrete_model_plan(graph, model, vars, path, max_time, figsize=(20000, 1000), start_time='2021.01.01 00:00:00', json_name='plan'):
    plan = ''
    units_files = [os.path.join(path, f) for f in os.listdir(path) if 
                  os.path.isfile(os.path.join(path, f))
                  and f.find('Equipment') != -1]
    units = [data_formats_lib.load_from_file(f) for f in units_files]
    df = pd.DataFrame()
    plan_json = data_formats_lib.create_object(json_name+'.json', obj_type='Plan')
    del plan_json['Job'] # delete empty job, added in constructor for template
    for u in units:
        plan += 'Unit '+u['id']+':\n'
        tasks = dict()
        for name, j in vars[u['id']].items():
            j_list = [(i, j[i]['batch'].solution_value()) for i in range(max_time) if i in j.keys() and j[i]['binary'].solution_value()]
            for t in j_list:
                tasks[t[0]] = (name, round(t[1], 2), int(graph[name]['vertex']['Duration']))
        
        for t in range(max_time):
            if t in tasks.keys():
                start = pd.to_datetime(start_time) + pd.DateOffset(hours=t)
                finish = start + pd.DateOffset(hours=tasks[t][2])
                df = df.append(dict(
                     Unit=u['id'], Start=str(start), Finish=str(finish), Task=tasks[t][0], Batch=str(tasks[t][1]), text=str(tasks[t][1])
                    ), ignore_index=True)
                job = data_formats_lib.create_object(u['id'] + '-' + tasks[t][0]+'-'+str(t), tasks[t][0], obj_type='Job')
                data_formats_lib.add_properties(job, 'Equipment', u['id'], True)
                data_formats_lib.add_properties(job, 'Batch size', str(tasks[t][1]), True)
                data_formats_lib.add_properties(job, 'Start time', str(t), True)
                data_formats_lib.add_properties(job, 'End time', str(t + tasks[t][2]), True)
                data_formats_lib.add_properties(plan_json, 'Job', job)
                plan += str(t)+' : '+tasks[t][0]+' with batch = '+str(tasks[t][1])+'\n'
        plan += '\n'
        
    plan += 'Stock:\n'
    for v in graph.values():
        if v['type'] == 'material':
            plan += v['vertex']['id']+':\n'
            for t in range(max_time):
                if t in vars[v['vertex']['id']].keys():
                    plan += str(t)+' : '+str(vars[v['vertex']['id']][t].solution_value())+'\n'
        plan += '\n'
    plan += '\n'
    plan += 'Model size:\n'
    plan += str(model.NumVariables()) + ' parameters\n'
    plan += str(model.NumConstraints()) + ' constraints\n'
    
    width = figsize[0]
    height = figsize[1]
    fig = px.timeline(df, x_start="Start", x_end="Finish", y="Unit", color="Task",width=width, height=height, text='text')
    fig.update_annotations(align='center')
    fig.update_yaxes(autorange="reversed")
    fig.update_xaxes(tickformat="%H:00",  dtick=60*60*1000)
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="left",
        x=0.01
    ))
    fig.update_layout(
    font=dict(
        family="Courier New, monospace",
        size=50
    ))
    
    return plan, fig, plan_json


def lp_heuristics_plan(graph, model, vars, tasks, path, max_time, figsize=(20000, 1000), start_time='2021.01.01 00:00:00', json_name='plan'):
    plan = ''
    plan_json = data_formats_lib.create_object(json_name+'.json', obj_type='Plan')
    del plan_json['Job'] # delete empty job, added in constructor for template
    units_files = [os.path.join(path, f) for f in os.listdir(path) if
                   os.path.isfile(os.path.join(path, f))
                   and f.find('Equipment') != -1]
    units = [data_formats_lib.load_from_file(f) for f in units_files]
    df = pd.DataFrame()
    for u in units:
        plan += 'Unit ' + u['id'] + ':\n'
        n = 0
        for task in tasks[u['id']]:
            t_start = vars[u['id']][n]['start'].solution_value()
            t_finish = vars[u['id']][n]['finish'].solution_value()
            start = pd.to_datetime(start_time) + pd.DateOffset(hours=t_start)
            finish = pd.to_datetime(start_time) + pd.DateOffset(hours=t_finish)
            df = df.append(dict(
                Unit=u['id'], Start=str(start), Finish=str(finish), Task=task[2]['id'], Batch=str(round(task[1], 2)),
                text=str(round(task[1], 2))
            ), ignore_index=True)
            job = data_formats_lib.create_object(u['id'] + '-' + task[2]['id'] +'-'+str(t_start), task[2]['id'], obj_type='Job')
            data_formats_lib.add_properties(job, 'Equipment', u['id'], True)
            data_formats_lib.add_properties(job, 'Batch size', str(task[1]), True)
            data_formats_lib.add_properties(job, 'Start time', str(t_start), True)
            data_formats_lib.add_properties(job, 'End time', str(t_finish), True)
            data_formats_lib.add_properties(plan_json, 'Job', job)
            plan += str(t_start) + ' : ' + task[2]['id'] + ' with batch = ' + str(task[1]) + '\n'

            n += 1

    plan += 'Stock:\n'
    for v in graph.values():
        if v['type'] == 'material':
            plan += v['vertex']['id'] + ':\n'
            for t in range(max_time):
                if t in vars[v['vertex']['id']].keys():
                    plan += str(t) + ' : ' + str(vars[v['vertex']['id']][t].solution_value()) + '\n'
        plan += '\n'
    plan += '\n'
    plan += 'Model size:\n'
    plan += str(model.NumVariables()) + ' parameters\n'
    plan += str(model.NumConstraints()) + ' constraints\n'

    width = figsize[0]
    height = figsize[1]
    fig = px.timeline(df, x_start="Start", x_end="Finish", y="Unit", color="Task", width=width, height=height, text='text')
    fig.update_annotations(align='center')
    fig.update_yaxes(autorange="reversed")
    fig.update_xaxes(tickformat="%H:00", dtick=60 * 60 * 1000)
    fig.update_layout(legend=dict(
        yanchor="top",
        y=0.99,
        xanchor="left",
        x=0.01
    ))
    fig.update_layout(
        font=dict(
            family="Courier New, monospace",
            size=50
        ))

    return plan, fig, plan_json
