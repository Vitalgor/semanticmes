# -*- coding: utf-8 -*-

import argparse
import lp_heuristics_model
import mip

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Scheduling models.')
    parser.add_argument('--path_to_data', type=str,required=True,
                    help='path to folder with model json files')
    parser.add_argument('--model_type', type=str, default='simple',
                    help='model type')
    parser.add_argument('--output_path', type=str,
                    help='folder where to store results', default='')
    args = parser.parse_args()
    path = args.path_to_data
    graph = lp_heuristics_model.build_graph(path)
    plan = fig = None
    if args.model_type == 'simple':
        model, vars = lp_heuristics_model.build_simple_discrete_model(graph, path)
        model.optimize()
        if model.num_solutions:
            plan, fig = lp_heuristics_model.simple_discrete_model_plan(graph, model, vars, path)
    if plan != None:
        with open(args.output_path + 'plan.txt', 'w+') as f:
            f.write(plan)
        fig.write_image(args.output_path+"plan.png")
    