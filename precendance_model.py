# -*- coding: utf-8 -*-
import os

from ortools.linear_solver import pywraplp
import data_formats_lib

# if p = 1 then a <= b
def make_indicator_constraint(model, a, b, p, big_number=1000):
    model.Add(p <= (b - a)/big_number + 1)

def build_precendance_model(graph, path, max_process):
    model = pywraplp.Solver.CreateSolver('SCIP')
    
    process_variables = dict()
    
    units_files = [os.path.join(path, f) for f in os.listdir(path) if 
                      os.path.isfile(os.path.join(path, f))
                      and f.find('Equipment') != -1]
    units = [data_formats_lib.load_from_file(f) for f in units_files]
    
    segment_files = [os.path.join(path, f) for f in os.listdir(path) if 
                      os.path.isfile(os.path.join(path, f))
                      and f.find('ProcessSegment') != -1]
    segments = [data_formats_lib.load_from_file(f) for f in segment_files]
    segments = {s['id']:s for s in segments}
    
    materials_operations = dict()
    operations_equipment = dict()
    equipment_operation = dict()
    
    for u in units:
        process_variables[u['id']] = dict()
        equipment_operation[u['id']] = list()
        
    for v in graph.values():
        if v['type'] == 'material':
            materials_operations[v['vertex']['id']] = dict()
            #operations, what produce material
            materials_operations[v['vertex']['id']]['inflow_operations'] = dict()
            #operations, what consume
            materials_operations[v['vertex']['id']]['outflow_operations'] = dict()
            materials_operations[v['vertex']['id']]['material'] = v['vertex']
        if v['type'] == 'operation':
            operations_equipment[v['vertex']['id']] = list()
    
    makespan = model.NumVar(0, model.infinity(), name='makespan')
    
    for v in graph.values():
        if v['type'] == 'operation':
            operation = v['vertex']
            segment = operation['ProcessSegmentId']
            equipment_class = segments[segment]['EquipmentSegmentSpecification']
            for u in units:
                if u['EquipmentClass'] == equipment_class:
                    equipment_operation[u['id']] += [operation['id']]
                    operations_equipment[operation['id']] += [u['id']]
                    
                    process_variables[u['id']][operation['id']] = dict()
                    var = process_variables[u['id']][operation['id']]
                    for num in range(max_process):
                        var[num] = dict()
                        name = u['id']+'-'+operation['id']+'-'+str(num)
                        
                        var[num]['active'] = model.IntVar(0, 1, name=name+'-active')
                        var[num]['batch'] = model.NumVar(0, model.infinity(), name=name+'-batch')
                        var[num]['begin_time'] = model.NumVar(0, model.infinity(), name=name+'-begin')
                        var[num]['end_time'] = model.NumVar(0, model.infinity(), name=name+'-end')
                        
                        model.Add(var[num]['end_time'] == var[num]['begin_time'] +\
                               int(operation['Duration']))
                        if num > 0:
                           model.Add(var[num]['begin_time'] >= var[num-1]['end_time'])
                           
                        make_indicator_constraint(model, var[num]['end_time'], makespan, var[num]['active'])
                           
                        model.Add(var[num]['batch'] <= var[num]['active']*int(u['max batch size']))
                        model.Add(var[num]['batch'] >= var[num]['active']*int(u['min batch size']))
                           
            for material in v['edges']:
                #quantity
                #print('searching q-product')
                field_name = operation['id']+'.ProductSpecification.'+material
                quantities = operation['ProductSpecification']
                if not isinstance(quantities, list):
                    quantities = [quantities]
                for quantity in quantities:
                    if quantity['id'] == field_name:
                        q = float(quantity['ValueString'])
                        #print('q = ',q)
                materials_operations[material]['inflow_operations'][operation['id']] = q    
                        
        if v['type'] == 'material':
            process_variables[v['vertex']['id']] = dict()
            for operation in v['edges']:
                #quantity
                #print('searching q-material')
                field_name = operation+'.MaterialSpecification.'+v['vertex']['id']
                quantities = graph[operation]['vertex']['MaterialSpecification']
                if not isinstance(quantities, list):
                    quantities = [quantities]
                for quantity in quantities:
                    if quantity['id'] == field_name:
                        q = float(quantity['ValueString'])
                        #print('q = ',q)
                materials_operations[v['vertex']['id']]['outflow_operations'][operation] = q
                
        
        
    #precendance variables
    # for every unit
    
    for u in units:
        var = process_variables[u['id']]
        for operation1 in equipment_operation[u['id']]:
            for num1 in range(max_process):
                for operation2 in equipment_operation[u['id']]:
                    if operation1 == operation2:
                        continue
                    for num2 in range(max_process):
                        key = (u['id'], operation1, num1, operation2, num2, 'strict')
                        name = str(key)+'-precendance'
                        var[key] = model.IntVar(0, 1, name=name)
                        make_indicator_constraint(model, var[operation1][num1]['end_time'], var[operation2][num2]['begin_time'], var[key])
                        make_indicator_constraint(model, var[operation2][num2]['end_time'], var[operation1][num1]['begin_time'], 1-var[key])

    #for operations that use and produce same material
    for material, operations in materials_operations.items():
        #material consuming
        for operation in operations['outflow_operations'].keys():
            for u in operations_equipment[operation]:
                for num1 in range(max_process):
                    flow = int(operations['material']['initial stock'])
                    for other_operation in operations['inflow_operations'].keys():
                        for other_u in operations_equipment[other_operation]:
                            for num2 in range(max_process):
                                #make variable for precendance
                                key = (material, operation, num1, other_operation, 'b-e')
                                name = str(key)+'-precendance'
                                process_variables[key] = model.IntVar(0, 1, name=name)
                                operation_begin = process_variables[u][operation][num1]['end_time']
                                other_operation_end = process_variables[other_u][other_operation][num2]['end_time']
                                make_indicator_constraint(model, other_operation_end, operation_begin, process_variables[key])
                                make_indicator_constraint(model, operation_begin + 1, other_operation_end, 1 - process_variables[key])
                                name = str(key)+'-flow'
                                #batch is how much material was transmitted
                                #it is equal to batch or to zero depend on precendance
                                precendance_batch = model.NumVar(0, model.infinity(), name=name)
                                batch = process_variables[other_u][other_operation][num2]['batch']
                                make_indicator_constraint(model, precendance_batch, batch, process_variables[key])
                                make_indicator_constraint(model, batch, precendance_batch, process_variables[key])
                                
                                make_indicator_constraint(model, precendance_batch, 0, 1 - process_variables[key])
                                make_indicator_constraint(model, 0, precendance_batch, 1 - process_variables[key])
                                
                                flow = flow + precendance_batch*operations['inflow_operations'][other_operation]
                                
                    for other_operation in operations['outflow_operations'].keys():
                        for other_u in operations_equipment[other_operation]:
                            for num2 in range(max_process):
                                #make variable for precendance
                                key = (material, operation, num1, other_operation, 'e-b')
                                name = str(key)+'-precendance'
                                process_variables[key] = model.IntVar(0, 1, name=name)
                                operation_begin = process_variables[u][operation][num1]['end_time']
                                other_operation_begin = process_variables[other_u][other_operation][num2]['end_time']
                                make_indicator_constraint(model, other_operation_begin, operation_begin, process_variables[key])
                                make_indicator_constraint(model, operation_begin + 1, other_operation_begin, 1 - process_variables[key])
                                name = str(key)+'-flow'
                                #batch is how much material was transmitted
                                #it is equal to batch or to zero depend on precendance
                                precendance_batch = model.NumVar(0, model.infinity(), name=name)
                                batch = process_variables[other_u][other_operation][num2]['batch']
                                make_indicator_constraint(model, precendance_batch, batch, process_variables[key])
                                make_indicator_constraint(model, batch, precendance_batch, process_variables[key])
                                
                                make_indicator_constraint(model, precendance_batch, 0, 1 - process_variables[key])
                                make_indicator_constraint(model, 0, precendance_batch, 1 - process_variables[key])
                                
                                flow = flow - precendance_batch*operations['outflow_operations'][other_operation]
                    
                    model.Add(flow >= 0)
                    model.Add(flow <= int(graph[material]['vertex']['max stock']))
        
        #material producing
        produced = 0
        for operation in operations['inflow_operations'].keys():
            for u in operations_equipment[operation]:
                for num1 in range(max_process):
                    batch = process_variables[u][operation][num1]['batch']
                    quantity = operations['inflow_operations'][operation]
                    produced = produced + batch*quantity
                    flow = int(operations['material']['initial stock'])
                    for other_operation in operations['inflow_operations'].keys():
                        for other_u in operations_equipment[other_operation]:
                            for num2 in range(max_process):
                                #make variable for precendance
                                key = (material, operation, num1, other_operation, 'e-e')
                                name = str(key)+'-precendance'
                                process_variables[key] = model.IntVar(0, 1, name=name)
                                operation_end = process_variables[u][operation][num1]['end_time']
                                other_operation_end = process_variables[other_u][other_operation][num2]['end_time']
                                make_indicator_constraint(model, other_operation_end, operation_end, process_variables[key])
                                make_indicator_constraint(model, operation_end + 1, other_operation_end, 1 - process_variables[key])
                                name = str(key)+'-flow'
                                #batch is how much material was transmitted
                                #it is equal to batch or to zero depend on precendance
                                precendance_batch = model.NumVar(0, model.infinity(), name=name)
                                batch = process_variables[other_u][other_operation][num2]['batch']
                                make_indicator_constraint(model, precendance_batch, batch, process_variables[key])
                                make_indicator_constraint(model, batch, precendance_batch, process_variables[key])
                                
                                make_indicator_constraint(model, precendance_batch, 0, 1 - process_variables[key])
                                make_indicator_constraint(model, 0, precendance_batch, 1 - process_variables[key])
                                
                                flow = flow + precendance_batch*operations['inflow_operations'][other_operation]
                                
                    for other_operation in operations['outflow_operations'].keys():
                        for other_u in operations_equipment[other_operation]:
                            for num2 in range(max_process):
                                #make variable for precendance
                                key = (material, operation, num1, other_operation, 'e-b')
                                name = str(key)+'-precendance'
                                process_variables[key] = model.IntVar(0, 1, name=name)
                                operation_end = process_variables[u][operation][num1]['end_time']
                                other_operation_begin = process_variables[other_u][other_operation][num2]['end_time']
                                make_indicator_constraint(model, other_operation_begin, operation_end, process_variables[key])
                                make_indicator_constraint(model, operation_end + 1, other_operation_begin, 1 - process_variables[key])
                                name = str(key)+'-flow'
                                #batch is how much material was transmitted
                                #it is equal to batch or to zero depend on precendance
                                precendance_batch = model.NumVar(0, model.infinity(), name=name)
                                batch = process_variables[other_u][other_operation][num2]['batch']
                                make_indicator_constraint(model, precendance_batch, batch, process_variables[key])
                                make_indicator_constraint(model, batch, precendance_batch, process_variables[key])
                                
                                make_indicator_constraint(model, precendance_batch, 0, 1 - process_variables[key])
                                make_indicator_constraint(model, 0, precendance_batch, 1 - process_variables[key])
                                
                                flow = flow - precendance_batch*operations['outflow_operations'][other_operation]
                    
                    if not isinstance(flow, int):
                        model.Add(flow >= 0)
                        model.Add(flow <= int(operations['material']['max stock']))
            
        demand = int(graph[material]['vertex']['demand'])
        if demand > 0:
            model.Add(produced >= demand)
            
    model.Minimize(makespan)
    
    return model, process_variables